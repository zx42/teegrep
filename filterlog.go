package teegrep

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"runtime"
	"strings"
	"sync"

	"github.com/pkg/errors"
)

type (
	rxFilterT struct {
		rxL                  []Matcher
		allOut               bool
		numLinesPerWorkBatch int

		workC         chan workItemT
		resultC       chan workResultT
		finishListenC chan error

		matchOut    *outBufferT
		nonmatchOut *outBufferT

		wg sync.WaitGroup
	}
	workItemT struct {
		idx   int
		lines []string
	}
	workResultT struct {
		idx                 int
		matches, nonmatches []string
	}
)

const (
	OutNone = iota
	OutMatch
	OutNonMatch
	OutAll
)

var (
	numThreads = runtime.NumCPU() + 2
)

func NewFilter(
	rxL []Matcher,
	matchResultFName string,
	nonmatchResultFName string,
	outMode int,
	numLinesPerWorkBatch int,
) (rxf *rxFilterT, err error) {
	var (
		matchResultF, nonmatchResultF io.WriteCloser
		matchOutF, nonmatchOutF       io.Writer
	)

	if matchResultFName != "" {
		matchResultF, err = os.Create(matchResultFName)
		if err != nil {
			return nil, errors.Wrap(err, "Creating match result file")
		}
	}
	if nonmatchResultFName != "" {
		nonmatchResultF, err = os.Create(nonmatchResultFName)
		if err != nil {
			return nil, errors.Wrap(err, "Creating non-match result file")
		}
	}

	switch outMode {
	case OutMatch:
		matchOutF = os.Stdout
	case OutNonMatch:
		nonmatchOutF = os.Stdout
	}

	rxf = &rxFilterT{
		rxL:                  rxL,
		allOut:               outMode == OutAll,
		matchOut:             NewOutBuffer(matchResultF, matchOutF),
		nonmatchOut:          NewOutBuffer(nonmatchResultF, nonmatchOutF),
		numLinesPerWorkBatch: numLinesPerWorkBatch,
		workC:                make(chan workItemT),
		resultC:              make(chan workResultT),
		finishListenC:        make(chan error),
	}

	go rxf.listen()

	for i := 0; i < numThreads; i++ {
		rxf.wg.Add(1)
		go func() {
			rxf.work()
		}()

	}

	return rxf, nil
}

func (rxf *rxFilterT) Filter(inputF io.Reader, outputF io.Writer) error {
	wi := workItemT{
		lines: make([]string, 0, rxf.numLinesPerWorkBatch+1),
	}
	logScnr := bufio.NewScanner(inputF)
	for logScnr.Scan() {
		line := strings.TrimSpace(logScnr.Text())

		if rxf.allOut {
			fmt.Fprintln(outputF, line)
		}

		if len(wi.lines) >= rxf.numLinesPerWorkBatch {
			rxf.workC <- wi
			wi.idx++
			wi.lines = make([]string, 0, rxf.numLinesPerWorkBatch+1)
		}
		wi.lines = append(wi.lines, line)
	}
	if logScnr.Err() != nil {
		return errors.Wrap(logScnr.Err(), "Reading input stream")
	}
	rxf.workC <- wi
	close(rxf.workC)
	rxf.wg.Wait()
	close(rxf.resultC)
	err := <-rxf.finishListenC
	close(rxf.finishListenC)
	return err
}

func (rxf *rxFilterT) Close() (err error) {
	err = rxf.matchOut.Close()
	err2 := rxf.nonmatchOut.Close()
	if err == nil {
		err = err2
	}
	return err
}

func (rxf *rxFilterT) listen() {
	openChannels := 1
	for openChannels > 0 {
		select {
		case wr, ok := <-rxf.resultC:
			if ok {
				rxf.matchOut.Push(wr.idx, wr.matches)
				rxf.nonmatchOut.Push(wr.idx, wr.nonmatches)
			} else {
				openChannels--
			}
		}
	}
	rxf.finishListenC <- rxf.Close()
}

func (rxf *rxFilterT) work() {
	for wi := range rxf.workC {
		wr := rxf.workOnItem(wi)
		rxf.resultC <- wr
	}
	rxf.wg.Done()
}

func (rxf *rxFilterT) workOnItem(wi workItemT) workResultT {
	wr := workResultT{
		idx:        wi.idx,
		matches:    make([]string, 0, len(wi.lines)),
		nonmatches: make([]string, 0, len(wi.lines)),
	}

	for _, line := range wi.lines {
		match := false
		for _, rx := range rxf.rxL {
			match = rx.MatchString(line)
			if match {
				wr.matches = append(wr.matches, line)
				break
			}
		}
		if !match {
			wr.nonmatches = append(wr.nonmatches, line)
		}
	}
	return wr
}
