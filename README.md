# TeeGrep

Yet another grep like tool.
It filters stdin and copies matching or non-matching input lines to stdout.
It collects matching or non-matching lines in output files.
It can process big inputs and uses as many CPU cores as possible.
The regular expressions to filter on can be in a file and therefore can exceed the command line size limitations.

## Usage

To see a usage message just run `teegrep -h`.
You will see something like:
```
teegrep is a command line tool to filter an input stream.
It always expects text data on it's standard input.
E.g. to list all linux users who can login:

	cat /etc/passwd | teegrep -e 'bin/nologin$' -e 'bin/false$' -output non-match

Usage of teegrep:
  -batch-size int
    	size of a work item in lines (default 1000)
  -e value
    	regular expression to filter on.
    	Can be used multiple times.
    	If it starts with '@' it will be interpreted as the name of a file
    	containing regular expressions each on a line
  -ignore-case
    	ignore case for all regular expressions
  -match string
    	output file for storing the lines which match any of the expressions
  -non-match string
    	output file for storing the lines which DO NOT match any of the expressions
  -output string
    	Choice:
    		- 'all'      : copy complete input to stdout
    		- 'match'    : copy only matching lines to stdout
    		- 'non-match': copy only non matching lines to stdout
    		- 'none'     : copy nothing to stdout
    	if not specified:
    		- if '-match' is not used: copy all matching lines to stdout
    		- if '-match' is used and '-non-match' is not used: copy all non-matching lines to stdout
    		- if '-match' and '-non-match' are used: copy nothing to stdout
    	
```
You can use single(`-`) or double (`--`) dashes to start command line options.

## Installation

Currently there are no binary releases available. To install it you need to have [Go](https://golang.org) 1.12+ installed.
Then you can just get it:
```
go get -u gitlab.com/zx42/teegrep
```
and run it from ~/go/bin (make sure it's in your `PATH`).
