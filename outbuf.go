package teegrep

import (
	"fmt"
	"io"
)

type (
	outBufferT struct {
		outIdx    int
		strSlices [][]string
		outFL     []io.Writer
	}
)

func NewOutBuffer(outFL ...io.Writer) *outBufferT {
	ob := &outBufferT{}
	for _, ow := range outFL {
		if ow != nil {
			ob.outFL = append(ob.outFL, ow)
		}
	}
	return ob
}

func (ob *outBufferT) Push(idx int, lines []string) {
	if lines == nil {
		panic("tried to push nil")
	}
	if len(ob.outFL) == 0 {
		return
	}

	// for idx-ob.outIdx >= len(ob.strSlices) {
	for idx >= len(ob.strSlices) {
		ob.strSlices = append(ob.strSlices, nil)
	}
	ob.strSlices[idx] = lines

	ob.writeOut()
}

func (ob *outBufferT) writeOut() {
	if len(ob.outFL) == 0 {
		return
	}

	// startIdx := ob.outIdx
	for ob.outIdx < len(ob.strSlices) && ob.strSlices[ob.outIdx] != nil {
		for _, line := range ob.strSlices[ob.outIdx] {
			for _, ow := range ob.outFL {
				fmt.Fprintln(ow, line)
			}
		}
		ob.strSlices[ob.outIdx] = nil
		ob.outIdx++
	}
	// if ob.outIdx == len(ob.strSlices)-1 {
	// 	ob.strSlices = make([][]string, 0, 10)
	// }
}

func (ob *outBufferT) String() string {
	m := make([]int, 0, 3)
	for _, sl := range ob.strSlices {
		if sl == nil {
			m = append(m, -1)
		} else {
			m = append(m, len(sl))
		}
	}

	return fmt.Sprintf("outBufferT{idx=%d, %v}", ob.outIdx, m)
}

func (ob *outBufferT) Close() (err error) {
	if len(ob.outFL) == 0 {
		return
	}

	ob.writeOut()
	// if len(ob.strSlices) > 0 {
	// 	msg := fmt.Sprintf("outBufferT.Close(): still string slices left: %s", ob)
	// 	panic(msg)
	// }
	for _, ss := range ob.strSlices {
		if ss != nil {
			msg := fmt.Sprintf("outBufferT.Close(): still string slices left: %s", ob)
			panic(msg)
		}
	}

	for _, ow := range ob.outFL {
		oc, ok := ow.(io.Closer)
		if ok {
			err2 := oc.Close()
			if err == nil {
				err = err2
			}
		}
	}
	ob.outFL = nil
	return err
}
