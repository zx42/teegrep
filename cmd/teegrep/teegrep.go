package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"net/http"
	_ "net/http/pprof"

	"gitlab.com/zx42/teegrep"
)

var (
	exprL                strSlice
	matchResultFName     string
	nonmatchResultFName  string
	outputChoice         = ""
	numLinesPerWorkBatch = 1000
	ignoreCase           = false
	httpDebugPort        = 0
)

func init() {
	flag.Var(&exprL, "e", `regular expression to filter on.
Can be used multiple times.
If it starts with '@' it will be interpreted as the name of a file
containing regular expressions each on a line`)
	flag.StringVar(&matchResultFName, "match", matchResultFName,
		`output file for storing the lines which match any of the expressions`)
	flag.StringVar(&nonmatchResultFName, "non-match", nonmatchResultFName,
		`output file for storing the lines which DO NOT match any of the expressions`)
	flag.StringVar(&outputChoice, "output", outputChoice, `Choice:
	- 'all'      : copy complete input to stdout
	- 'match'    : copy only matching lines to stdout
	- 'non-match': copy only non matching lines to stdout
	- 'none'     : copy nothing to stdout
if not specified:
	- if '-match' is not used: copy all matching lines to stdout
	- if '-match' is used and '-non-match' is not used: copy all non-matching lines to stdout
	- if '-match' and '-non-match' are used: copy nothing to stdout
`)
	flag.IntVar(&numLinesPerWorkBatch, "batch-size", numLinesPerWorkBatch, "size of a work item in lines")
	flag.BoolVar(&ignoreCase, "ignore-case", ignoreCase, "ignore case for all regular expressions")
	flag.IntVar(&httpDebugPort, "http-debug-port", httpDebugPort, "port so serve http pprof data on")
}

const (
	usageMsgFmt = `teegrep is a command line tool to filter an input stream.
It always expects text data on it's standard input.
E.g. to list all linux users who can login:

	cat /etc/passwd | teegrep -e 'bin/nologin$' -e 'bin/false$' -output non-match

Usage of %s:
`
)

func main() {
	flag.Usage = printUsage
	flag.Parse()
	if len(exprL) == 0 {
		flag.Usage()
		return
	}

	rxL, err := teegrep.ReadRegexFile(exprL, ignoreCase, false)
	if err != nil {
		log.Fatalf("teegrep: %s", err)
		return
	}

	outMode := getOutMode(outputChoice)

	rxf, err := teegrep.NewFilter(rxL, matchResultFName, nonmatchResultFName, outMode, numLinesPerWorkBatch)
	if err != nil {
		log.Fatalf("teegrep: creating filter: %s", err)
		return
	}

	if httpDebugPort > 0 {
		go func() {
			addrS := fmt.Sprintf("localhost:%d", httpDebugPort)
			log.Println(http.ListenAndServe(addrS, nil))
		}()
	}

	err = rxf.Filter(os.Stdin, os.Stdout)
	if err != nil {
		log.Fatalf("teegrep: filtering: %s", err)
		return
	}

	// err = rxf.Close()
	// if err != nil {
	// 	log.Fatalf("teegrep: closing: %s", err)
	// 	return
	// }
}

func getOutMode(outputChoice string) int {
	outMode := teegrep.OutMatch
	switch outputChoice {
	case "all":
		outMode = teegrep.OutAll
	case "match":
		outMode = teegrep.OutMatch
	case "non-match":
		outMode = teegrep.OutNonMatch
	case "none":
		outMode = teegrep.OutNone
	default:
		switch {
		case matchResultFName != "" && nonmatchResultFName == "":
			outMode = teegrep.OutNonMatch
		case matchResultFName != "" && nonmatchResultFName != "":
			outMode = teegrep.OutNone
		default:
			outMode = teegrep.OutMatch
		}
	}
	return outMode
}

func printUsage() {
	fmt.Fprintf(flag.CommandLine.Output(), usageMsgFmt, os.Args[0])
	flag.PrintDefaults()
}

// func serveHttp() {
// 	http.ListenAndServe()
// }
