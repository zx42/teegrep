package main

import (
	"errors"
)

type (
	strSlice []string
)

func (ss *strSlice) String() string {
	return "my string representation"
}

func (ss *strSlice) Set(value string) error {
	if value == "" {
		return errors.New("Empty option string")
	}
	*ss = append(*ss, value)
	return nil
}

// var myFlags arrayFlags

// func main() {
// 	flag.Var(&myFlags, "list1", "Some description for this param.")
// 	flag.Parse()
// }
