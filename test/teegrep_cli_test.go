package acceptance_test

import (
	"bytes"
	"io/ioutil"
	"os/exec"
	"strings"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gbytes"
	"github.com/onsi/gomega/gexec"
)

var _ = Describe("Tee Grep", func() {
	var (
		binaryPath string
		// session    *gexec.Session
	)

	BeforeSuite(func() {
		binaryPath = buildCliBin()
	})

	AfterSuite(func() {
		gexec.CleanupBuildArtifacts()
	})

	It("displays usage message with status code 0", func() {
		session := runCliBin(binaryPath)
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Err).Should(gbytes.Say(`^teegrep is a command line tool to filter an input stream.
It always expects text data on it's standard input.
E.g. to list all linux users who can login:

	cat /etc/passwd | teegrep -e 'bin/nologin$' -e 'bin/false$' -output non-match

Usage of`))
	})

	It("output default", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^b
$`))
	})

	It("output match", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-output", "match")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^b
$`))
	})

	It("output nonmatches", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-output", "non-match")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^a
c
$`))
	})

	It("output all", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-output", "all")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^a
b
c
$`))
	})

	It("output none", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-output", "none")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^$`))
	})

	It("creates a correct match file", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-match", "m.out")
		Eventually(session).Should(gexec.Exit(0))
		session.Wait()
		Expect("m.out").To(BeAnExistingFile())
		mB, err := ioutil.ReadFile("m.out")
		Expect(err).To(BeNil())
		mS := strings.TrimSpace(string(mB))
		Expect(mS).To(Equal("b"))
	})

	It("creates a correct non-match file", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-non-match", "nm.out")
		session.Wait()
		Eventually(session).Should(gexec.Exit(0))
		Expect("nm.out").To(BeAnExistingFile())
		nmB, err := ioutil.ReadFile("nm.out")
		Expect(err).To(BeNil())
		outputS := `a
c
`
		Expect(string(nmB)).To(Equal(outputS))
	})

	It("only match file and output non-match", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-match", "m.out")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^a
c
$`))
	})

	It("only non-match file and output match", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "b", "-non-match", "nm.out")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^b
$`))
	})

	It("filters multiple expressions", func() {
		inputS := `a
b
c
`
		session := runCliBinWithInput(inputS, binaryPath, "-e", "c", "-e", "b", "-e", "a", "-output", "non-match")
		Eventually(session).Should(gexec.Exit(0))
		Eventually(session.Out).Should(gbytes.Say(`^$`))
	})
})

func buildCliBin() string {
	binPath, err := gexec.Build("gitlab.com/zx42/teegrep/cmd/teegrep")
	Expect(err).NotTo(HaveOccurred())

	return binPath
}

func runCliBin(path string, args ...string) *gexec.Session {
	return runCliBinWithInput("", path, append(args, "-batch-size", "1")...)
}

func runCliBinWithInput(inS string, path string, args ...string) *gexec.Session {
	cmd := exec.Command(path, args...)
	if inS != "" {
		cmd.Stdin = bytes.NewBufferString(inS)
	}
	session, err := gexec.Start(cmd, GinkgoWriter, GinkgoWriter)
	// session, err := gexec.Start(cmd, GinkgoWriter, nil)
	Expect(err).NotTo(HaveOccurred())

	return session
}

func TestTest(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Test Suite")
}
