# Change Log of TeeGrep

## Unreleased

* ...

## To Do

### User Features

* Add -discard-before <RegEx>
* Add -discard-after <RegEx>

### Architecture + Quality Features

* ...

## History

### v0.0.2 - 2019-04-xx

#### Added

* Speed up matching of strings which are not regular expressions
* Architecture: Add pprof tracing

#### Fixed

* ...

#### Removed

* ...

### v0.0.1 - 2019-03-28

First working version
