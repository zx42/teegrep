package teegrep

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/pkg/errors"
)

type (
	Matcher interface {
		MatchString(line string) bool
	}
	stringMatcherT struct {
		s string
	}
)

func ReadRegexFile(regExSL []string, ignoreCase bool, joinRx bool) ([]Matcher, error) {
	rxSL := make([]string, 0, 10)
	for _, expr := range regExSL {
		if strings.HasPrefix(expr, "@") {
			regexOrListFName := expr[1:]
			regexOrListF, err := os.Open(regexOrListFName)
			if err != nil {
				return nil, errors.Wrap(err, "Opening RegEx list file")
			}
			defer regexOrListF.Close()

			s := bufio.NewScanner(regexOrListF)
			for s.Scan() {
				rxSL = appendRxS(rxSL, s.Text(), ignoreCase)
			}
			if s.Err() != nil {
				return nil, errors.Wrap(s.Err(), "Reading RegEx list file")
			}
		} else {
			rxSL = appendRxS(rxSL, expr, ignoreCase)
		}
	}

	rxL := make([]Matcher, 0, len(rxSL))
	if joinRx {
		rxLS := strings.Join(rxSL, ")|(")
		rxS := fmt.Sprintf("(%s)", rxLS)
		idRx, err := compileRX(rxS)
		if err != nil {
			return nil, errors.Wrapf(err, "Parsing regex '%s'", rxS)
		}
		rxL = append(rxL, idRx)
	} else {
		for _, rxS := range rxSL {
			idRx, err := compileRX(rxS)
			if err != nil {
				return nil, errors.Wrapf(err, "Parsing regex '%s'", rxS)
			}
			rxL = append(rxL, idRx)
		}
	}
	return rxL, nil
}

func appendRxS(sL []string, s string, ignoreCase bool) []string {
	s2 := strings.TrimSpace(s)
	if ignoreCase {
		s2 = fmt.Sprintf("(?i)%s", s2)
	}
	return append(sL, s2)
}

func compileRX(rxS string) (Matcher, error) {
	quotedRxS := regexp.QuoteMeta(rxS)
	if quotedRxS == rxS {
		return &stringMatcherT{s: rxS}, nil
	}
	return regexp.Compile(rxS)
}

func (sm *stringMatcherT) MatchString(line string) bool {
	return strings.Contains(line, sm.s)
}
